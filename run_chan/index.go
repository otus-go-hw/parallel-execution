package run_chan

import (
	"errors"
	"fmt"
)

func Run(tasks []func() error, n int, m int) (err error) {
	if n < 0 || m < 0 {
		return errors.New("n or m have a negative value")
	}
	var chanSucces, chanErr int
	queueCh := make(chan func() error, len(tasks))
	doneCh := make(chan bool)
	killCh := make(chan bool)

	for i := 1; i <= n; i++ {
		go worker(i, queueCh, doneCh, killCh)
	}

	for _, task := range tasks {
		queueCh <- task
	}

	for range tasks {
		done := <-doneCh
		if !done {
			chanErr++
			if chanErr > m {
				err = errors.New("error limit exceeded")
				break
			}

			if chanErr == m {
				err = errors.New("too many mistakes")
				if chanErr+chanSucces > n+m {
					err = errors.New("The number of tasks and errors does not match ")
				}
				break
			}
		}
		chanSucces++
	}

	close(killCh)
	return err
}

func worker(n int, queueCh <-chan func() error, doneCh chan<- bool, killCh <-chan bool) {
	fmt.Printf("Run worker: count %d \n", n)
	for {
		select {
		case task := <-queueCh:
			fmt.Printf("add %d task...\n", n)
			err := task()
			doneCh <- err == nil
		case <-killCh:
			fmt.Printf("add %d stopped\n", n)
			return
		}
	}
}
