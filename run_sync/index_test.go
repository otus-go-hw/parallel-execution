package run_sync

import (
	"errors"
	"fmt"
	"testing"
)

func TestInvalidNumParams(t *testing.T) {
	tasks := []func() error{
		func() error { fmt.Println("task #1"); return nil },
		func() error { fmt.Println("task #2"); return nil },
		func() error { fmt.Println("task #3"); return nil },
		func() error { fmt.Println("task #4"); return nil },
		func() error { fmt.Println("task #5"); return nil },
		func() error { fmt.Println("task #6"); return nil },
		func() error { fmt.Println("task #7"); return nil },
		func() error { fmt.Println("task #8"); return nil },
	}
	n := -1
	m := 8
	err := Run(tasks, n, m)
	if err == nil {
		t.Fatalf("bad result expected error")
	}
}



func TestInvalidErrParams(t *testing.T) {
	tasks := []func() error{
		func() error { fmt.Println("task #1"); return nil },
		func() error { fmt.Println("task #2"); return nil },
		func() error { fmt.Println("task #3"); return nil },
		func() error { fmt.Println("task #4"); return nil },
		func() error { fmt.Println("task #5"); return nil },
		func() error { fmt.Println("task #6"); return nil },
		func() error { fmt.Println("task #7"); return nil },
		func() error { fmt.Println("task #8"); return nil },
	}
	n := 8
	m := -1
	err := Run(tasks, n, m)
	if err == nil {
		t.Fatalf("bad result expected error")
	}
}


func TestFails(t *testing.T) {
	tasks := []func() error{
		func() error {
			fmt.Println("error task #3 ")
			return errors.New("error task #3")
		},
		func() error {
			fmt.Println("error task #4 ")
			return errors.New("error task #4")
		},
		func() error {
			fmt.Println("error task #5 ")
			return errors.New("error task #5")
		},
		func() error {
			fmt.Println("task #1")
			return nil
		},
		func() error {
			fmt.Println("task #2")
			return nil
		},
	}
	n := 2
	m := 2
	err := Run(tasks, n, m)
	if err == nil {
		t.Fatalf(`expected an error`)
	}
}

func TestNullTasks(t *testing.T) {
	var tasks []func() error
	n, m := 5, 5
	err := Run(tasks, n, m)
	if err != nil {
		t.Fatalf("bad result, %v", err)
	}
}

func TestOk(t *testing.T) {
	tasks := []func() error{
		func() error {
			fmt.Println("task #1")
			return nil
		},
		func() error {
			fmt.Println("task #2")
			return nil
		},
		func() error {
			fmt.Println("task #3")
			return nil
		},
		func() error {
			fmt.Println("task #4")
			return nil
		},
		func() error {
			fmt.Println("task #5")
			return nil
		},
	}
	err := Run(tasks, 2, 2)
	if err != nil {
		t.Fatalf(`TestOK: %s`, err)
	}
}

func TestCountTasksThen(t *testing.T) {
	tasks := []func() error{
		func() error { fmt.Println("task #1"); return nil },
		func() error { fmt.Println("task #2"); return nil },
		func() error { fmt.Println("task #3"); return nil },
		func() error { fmt.Println("task #4"); return nil },
		func() error { fmt.Println("task #5"); return nil },
		func() error { fmt.Println("task #6"); return nil },
		func() error { fmt.Println("task #7"); return nil },
		func() error { fmt.Println("task #8"); return nil },
	}
	n, m := 8, 2
	err := Run(tasks, n, m)
	if err != nil {
		t.Fatalf("bad result, %v", err)
	}
}
