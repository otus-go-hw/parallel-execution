package run_sync

import (
	"errors"
	"sync"
)

func Run(tasks []func() error, n int, m int) (err error) {
	if n < 0 || m < 0 {
		return errors.New("n or m have a negative value")
	}
	ch := make(chan struct{}, n)
	mu := sync.Mutex{}

	var wg sync.WaitGroup
	var wgSuccess, wgError int
	var drop bool

	for _, task := range tasks {
		ch <- struct{}{}
		if drop {
			err = errors.New("error limit exceeded")
			break
		}

		wg.Add(1)
		go func(task func() error) {
			defer wg.Done()
			defer func() {
				<-ch
			}()
			err := task()
			action(&wgSuccess, &wgError, &n, &m, &drop, &mu, err)
		}(task)
	}

	close(ch)
	wg.Wait()

	return err
}

func action(wgSuccess *int, wgError *int, n *int, m *int, drop *bool, mu *sync.Mutex, err error) {
	mu.Lock()
	defer mu.Unlock()

	if err != nil {
		*wgError++
		if *wgError >= *m {
			*drop = true
		}
		if *wgError > 0 && *wgError+*wgSuccess > *n+*m {
			*drop = true
		}
	} else {
		*wgSuccess++
	}
}
